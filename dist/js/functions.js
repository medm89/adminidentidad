function desvincular() {
    Swal.fire({
        title: 'Estas seguro de querer desvicular este dispositivo?',
        text: "Esta accion no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro!'
    }).then((result) => {
        if (result.value) {
            var parametros = { "operation": 1 };
            $.ajax({
                type: "POST",
                url: "controllers/deviceController.php",
                dataType: 'json',
                beforeSend: function(xhr) {},
                success: function(data) {
                    var res = data["respuesta"];
                    if (res['Error'] == "Si") {
                        Swal.fire(
                            'Upss!',
                            'No tiene dispositivos asociados a su cuenta actualmente',
                            'warning'
                        )
                    } else {
                        Swal.fire(
                            'Eliminado!',
                            'Se ha desvinculado tu dispositivo.',
                            'success'
                        );
                    }

                },
                error: function(data) {
                    Swal.fire({ icon: 'error', title: 'Oops...', text: data["respuesta"] });
                },
                complete: function(data) {
                    // console.log(data);
                    //return false; // Evitar ejecutar el submit del formulario.
                }
            });
        }
    })
}

$("#updatePassword").click(function() {
    $("#cancelPassword").trigger("click");
    event.preventDefault();
    var url = "controllers/changePasswordController.php";
    username = $("#email").val();
    password = $("#password1").val();
    var parametros = { "email": username, "password": password };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros, // Adjuntar los campos del formulario enviado.
        dataType: 'json',
        beforeSend: function(xhr) {
            // $('#formulario').html('<img class="ico-charging" src="/wp-content/themes/citylending/assets/images/loading_city.svg" />');
        },
        success: function(data) {
            json = data["respuesta"];
            if (json == 1) {
                Swal.fire({ icon: 'success', title: 'Muy bien!!', text: 'Se actualizo la contraseña ' });
            } else {
                Swal.fire({ icon: 'error', title: 'Oops...', text: 'Ocurrio un error, reportelo al administrador del sistema' });
            }
        },
        error: function(data) {
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'Las credenciales son incorrectas' });
        },
        complete: function(data) {
            //console.log(data);
            return false; // Evitar ejecutar el submit del formulario.
        }
    });
    return false;
});
$('#listUsers').on('show.bs.modal', function(event) {
    var url = "controllers/adminController.php";
    var parametros = { "operacion": 1 };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros, // Adjuntar los campos del formulario enviado.
        dataType: 'json',
        beforeSend: function(xhr) {
            // $('#formulario').html('<img class="ico-charging" src="/wp-content/themes/citylending/assets/images/loading_city.svg" />');
        },
        success: function(data) {
            json = data["respuesta"]['data'];
            var html = "";
            for (let index = 0; index < json.length; index++) {
                html += "<tr>";
                html += "<td>" + json[index]["email"] + "</td>";
                html += "<td><a class='fa fa-times' style='color:#650E81' onclick='delAdminuser(" + json[index]["id"] + ")' href='javascript:void(0)'></a><td>";
                html += "</tr>";
            }
            $("#usuarioAdmin").html(html);
        },
        error: function(data) {
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'Las credenciales son incorrectas' });
        },
        complete: function(data) {
            //console.log(data);
            // return false; // Evitar ejecutar el submit del formulario.
        }
    });

});

function delAdminuser(id) {
    Swal.fire({
        title: 'Estas seguro de querer eleminiar este usuario?',
        text: "Esta accion no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro!'
    }).then((result) => {
        if (result.value) {
            var parametros = { "operacion": 2, "code": id };
            $.ajax({
                type: "POST",
                url: "controllers/adminController.php",
                data: parametros, // Adjuntar los campos del formulario enviado.
                dataType: 'json',
                beforeSend: function(xhr) {},
                success: function(data) {
                    $("#cancelAdmin").trigger("click");
                    $("#btnListUser").trigger("click");
                    var res = data["respuesta"];
                    if (res['Error'] == "Si") {
                        Swal.fire(
                            'Upss!',
                            'No se puede eliminar el usuario.',
                            'warning'
                        )
                    } else {
                        Swal.fire(
                            'Eliminado!',
                            'Se ha eliminado el usuario.',
                            'success'
                        );
                    }

                },
                error: function(data) {
                    console.log(data);
                    Swal.fire({ icon: 'error', title: 'Oops...', text: data["respuesta"] });
                },
                complete: function(data) {
                    // console.log(data);
                    //return false; // Evitar ejecutar el submit del formulario.
                }
            });
        }
    })
}
$("#enviarMensaje").click(function() {
    $("#cerrarMensaje").trigger("click");
    event.preventDefault();
    var url = "controllers/mensajeController.php";
    var parametros = { "message": $("#message-text").val() };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros, // Adjuntar los campos del formulario enviado.
        dataType: 'json',
        beforeSend: function(xhr) {
            // $('#formulario').html('<img class="ico-charging" src="/wp-content/themes/citylending/assets/images/loading_city.svg" />');
        },
        success: function(data) {
            $("#message-text").val("")
            console.log(data);
            Swal.fire({ icon: 'success', title: 'Mensaje Enviado', text: 'Se envio su mensaje, nos estaremos contactando con usted pronto' });
        },
        error: function(data) {
            console.log(data);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo enviar su mensaje' });
        },
        complete: function(data) {
            //console.log(data);
            // return false; // Evitar ejecutar el submit del formulario.
        }
    });
});
$("#AgregarUsuarioAdmin").click(function() {
    $("#cancelAdmin").trigger("click");
    event.preventDefault();
    var url = "controllers/adminController.php";
    var parametros = { "email": $("#inputPassword2").val(), "operacion": 3 };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros, // Adjuntar los campos del formulario enviado.
        dataType: 'json',
        beforeSend: function(xhr) {
            // $('#formulario').html('<img class="ico-charging" src="/wp-content/themes/citylending/assets/images/loading_city.svg" />');
        },
        success: function(data) {
            console.log(data);
            Swal.fire({ icon: 'success', title: 'Usuario Agregado', text: 'Usuario agregado correctamente' });
            $("#inputPassword2").val("")
            $("#btnListUser").trigger("click");

        },
        error: function(data) {
            console.log(data);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo crear el usuario' });
        },
        complete: function(data) {

            // return false; // Evitar ejecutar el submit del formulario.
        }
    });
});
$("#btnListUser").click(function() {

});