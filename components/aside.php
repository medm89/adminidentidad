<?php

?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="dist/img/uninorte/logoBlanco.svg" alt="Uninorte Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">ID UNINORTE</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                      <img alt="foto" src="data:image/png;base64,<?php echo base64_encode(pack('H*',$_SESSION['foto1']).pack('H*',$_SESSION['foto2']).pack('H*',$_SESSION['foto3']).pack('H*',$_SESSION['foto4']));; ?>" width="100%" class="img-circle elevation-2"/>
                    </div>
                    <div class="info">
                        <a href="#" class="d-block text-capitalize text-wrap"><?php echo $_SESSION['nombre'];?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-header"> Marca celular</li>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-phone"></i>
                            <p>
                             <?php 
                             if(isset($_SESSION["user_phone_model"])){
                                 echo $_SESSION["user_phone_model"];
                             }?>
                             </p>
                          </a>
                        </li>
                        <li class="nav-header" style="padding-top: 0;"> Modelo celular</li>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-mobile"></i>
                            <p>
                             <?php 
                             if(isset($_SESSION["user_phone_brand"])){
                                 echo $_SESSION["user_phone_brand"];
                             }?>
                             </p>
                          </a>
                        </li>
                        <li class="nav-header" style="padding-top: 0;">Sistema operativo</li>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-mobile"></i>
                            <p>
                             <?php 
                             if(isset($_SESSION["user_phone_movilso"])){
                                 echo $_SESSION["user_phone_movilso"];
                             }?>
                             </p>
                          </a> 
                        </li>
                        <li class="nav-header" style="padding-top: 0;">versión sistema operativo</li>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-ellipsis-v"></i>
                            <p>
                             <?php 
                             if(isset($_SESSION["user_phone_versionso"])){
                                 echo "Ver. ".$_SESSION["user_phone_versionso"];
                             }?>
                             </p>
                          </a> 
                        </li>
                        <li class="nav-header" style="padding-top: 0;">última conexión dispositivo</li>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-ellipsis-v"></i>
                            <p>
                             <?php 
                             if(isset($_SESSION["user_last_access"])){
                                 echo $_SESSION["user_last_access"];
                             }?>
                             </p>
                          </a> 
                        </li>
                        
                        <!-- <li class="nav-item">
                            <a href="pages/gallery.html" class="nav-link">
                                <i class="nav-icon far fa-image"></i>
                                <p>
                                    Gallery
                                </p>
                            </a>
                        </li> -->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
