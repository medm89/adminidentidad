    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>ID UNINORTE| HOME</title>

     
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Theme Personal -->
    <link rel="stylesheet" href="dist/css/iduninorte.css">
    <!-- Personal style -->
    <link rel="stylesheet" href="dist/css/iduninorte.css">
    <!-- Font awsome -->
    <link rel="stylesheet" href="dist/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

  
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <?php 
     session_start();
     if(!isset($_SESSION['token'])){
       header('location:index.php');
     }
    ?>
