<div class="content-header">
 <div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark texto-head">Mis Servicios</h1>
    </div>
</div>
<div class="content-header p-0 bg-white border-top border-bottom">
    <div class="container-fluid">
        <div class="row mb-2">
            
            <div class="mr-auto ml-4">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="nav-link" href="home"><i class="fas fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item active"><a style="color:#333333;" href="index.php">Dashboard</a></li>
                </ol>
            </div>
            <div class="col-sm-6 d-flex float-right">
                <a class="nav-link" href="#">
                    <i class="fas fa-comments"></i> Soporte </a>
                <a class="nav-link" href="#">
                    <i class="fas fa-cog"></i> Configuración </a>
            </div>

        </div>
        <!-- /.row -->
    </div>
   <!-- /.row -->
   </div>
  <!-- /.container-fluid -->
</div>
