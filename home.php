<!DOCTYPE html>
<html lang="en">

<head>
  <?php include('components/linked.php');


  ?>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('components/navbar.php') ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include('components/aside.php') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <?php include('components/wrapper.php'); ?>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row row-cols-1 row-cols-md-2">
            <div class="col-md-8">
              <div class="row">

                <div class="col-md-4 cardUN">
                  <div class="card">
                    <div class="card-body text-center">
                      <a href="javascript:void()" onclick="desvincular()"><img src="dist/img/uninorte/desvincular.svg" alt=""></a>
                    </div>
                    <div class="card-footer">
                      <p class="card-text text-center"><span class="texto-title w-75 ">Desvincular dispositivo </span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-4 cardUN">
                  <div class="card">
                    <div class="card-body text-center">
                      <a href="javascript:void(0)"><img src="dist/img/uninorte/logs.svg" alt=""></a>
                    </div>
                    <div class="card-footer">
                      <p class="card-text text-center"><span class="texto-title w-75">Logs de usuario </span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-4 cardUN">
                  <div class="card">
                    <div class="card-body text-center">
                      <a href="javascript:void(0)"><img src="dist/img/uninorte/reportes.svg" alt=""></a>
                    </div>
                    <!-- <a href="javascript:void(0)"></a> -->
                    <div class="card-footer">
                      <p class="card-text text-center"><span class="texto-title w-75">Reportes </span></p>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">

                <div class="col-md-4 cardUN">
                  <div class="card">
                    <div class="card-body text-center">
                      <button id="btnListUser" type="button" class="nobutton" data-toggle="modal" data-target="#listUsers">
                        <img src="dist/img/uninorte/usuario.svg " alt="">
                      </button>
                    </div>
                    <div class="card-footer">
                      <p class="card-text text-center"><span class="texto-title w-75">Administrar usuarios </span></p>
                    </div>
                  </div>
                </div>

                <?php if (isset($_SESSION["regemail"])) { ?>
                  <?php if (explode("@", $_SESSION["regemail"])[1] != "uninorte.edu.co") { ?>
                    <div class="col-md-4 cardUN">
                      <div class="card">
                        <div class="card-body text-center">
                          <button type="button" class="nobutton" data-toggle="modal" data-target="#changePassword">
                            <img src="dist/img/uninorte/password.svg" alt=""></a>
                          </button>
                        </div>
                        <div class="card-footer">
                          <p class="card-text text-center"><span class="texto-title w-75">Cambiar contraseña </span></p>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>

                <div class="col-md-4 cardUN">
                  <div class="card">
                    <div class="card-body text-center">
                      <button type="button" class="nobutton" data-toggle="modal" data-target="#exampleModal" data-whatever="<?php echo $_SESSION["nombre"] ?>">
                        <img class="width:106px;" src="dist/img/uninorte/solicitud.svg" alt=""></button>
                    </div>
                    <div class="card-footer">
                      <p class="card-text text-center"><span class="texto-title w-75">Solicitud a carnetización</span></p>
                    </div>
                  </div>
                </div>
                <!-- /.col-md-6 -->
              </div>
            </div>
            <div class="col-md-4">
              <!-- INICIO timeline -->
              <!-- FIN de timeline -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- MODAL ENVIO DE SOLICITUDES  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Envio de solicitud</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Usuario:</label>
                <input type="text" class="form-control" id="recipient-name" name="recipient-name" readonly>
              </div>
              <div class="form-group">
                <label for="message-text" class="col-form-label">Mensaje:</label>
                <textarea rows="8" class="form-control" id="message-text" name="message-text"></textarea>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrarMensaje">Cerrar</button>
            <button type="button" class="btn btn-success" id="enviarMensaje">Enviar Mensaje</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.ENVIO DE SOLICITUDES -->
    <!-- MODAL CAMBIO DE CONTRASEÑA -->
    <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cambio de contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="passwordForm">
              <input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="Contraseña nueva " autocomplete="off">
              <div class="row">
                <div class="col-sm-6">
                  <span id="8char" class="fa fa-times" style="color:#FF0004;"></span> Por lo menos 8 caracteres de largo<br>
                  <span id="ucase" class="fa fa-times" style="color:#FF0004;"></span> Una Mayúscula
                </div>
                <div class="col-sm-6">
                  <span id="lcase" class="fa fa-times" style="color:#FF0004;"></span> Una minúscula<br>
                  <span id="num" class="fa fa-times" style="color:#FF0004;"></span> Un numero
                </div>
              </div>
              <input type="password" class="input-lg form-control" name="password2" id="password2" placeholder="Repetir contraseña" autocomplete="off">
              <div class="row">
                <div class="col-sm-12">
                  <span id="pwmatch" class="fa fa-times" style="color:#FF0004;"></span> Contraseñas coinciden
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelPassword">Cerrar</button>
            <button type="button" class="btn btn-success disabled" id="updatePassword">Actualizar contraseña</button>
          </div>
        </div>
      </div>
    </div>
    <!-- FIN CAMBIO DE CONTRASEÑA -->

    <!-- MODAL LISTADO DE USUARIO  -->
    <div class="modal fade" id="listUsers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Listado de usuarios</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body p-0">
            <div class="row">
               <div class="" style="margin: 0 auto; width:550px;">
               <form class="form-inline">
              <div class="form-group mb-2">
                <label for="staticEmail2" class="sr-only">Email</label>
                <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Agregar email nuevo admin">
              </div>
              <div class="form-group mx-sm-3 mb-2">
                <label for="inputPassword2" class="sr-only">Email</label>
                <input type="email" class="form-control" id="inputPassword2" placeholder="email uninorte">
              </div>
              <button type="button" id="AgregarUsuarioAdmin" class="btn btn-primary mb-2">Agregar</button>
            </form>
               </div>
            </div>
            <div class="row">
              <div class="col-12">
                <table id="table-list-user" class="table">
                  <thead>
                    <tr>
                      <th>Usuario</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody id="usuarioAdmin">
             
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelAdmin">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- FIN CAMBIO DE CONTRASEÑA -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2020.</strong> Todos los derechos reservados.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE -->
  <script src="dist/js/adminlte.js"></script>

  <!-- OPTIONAL SCRIPTS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <script src="dist/js/demo.js"></script>
  <script src="dist/js/pages/dashboard3.js"></script>
  <script src="dist/js/functions.js"></script>
</body>
<script>
  $('#exampleModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('Envio de Solicitud ' + recipient)
    modal.find('.modal-body input').val(recipient)
    modal.find('.modal-body textarea').val("")
  });

  $("input[type=password]").keyup(function() {
    var ucase = new RegExp("[A-Z]+");
    var lcase = new RegExp("[a-z]+");
    var num = new RegExp("[0-9]+");

    if ($("#password1").val().length >= 8) {
      $("#8char").removeClass("fa-times");
      $("#8char").addClass("fa-check");
      $("#8char").css("color", "#00A41E");
    } else {
      $("#8char").removeClass("fa-check");
      $("#8char").addClass("fa-times");
      $("#8char").css("color", "#FF0004");
    }

    if (ucase.test($("#password1").val())) {
      $("#ucase").removeClass("fa-times");
      $("#ucase").addClass("fa-check");
      $("#ucase").css("color", "#00A41E");
    } else {
      $("#ucase").removeClass("fa-check");
      $("#ucase").addClass("fa-times");
      $("#ucase").css("color", "#FF0004");
    }

    if (lcase.test($("#password1").val())) {
      $("#lcase").removeClass("fa-times");
      $("#lcase").addClass("fa-check");
      $("#lcase").css("color", "#00A41E");
    } else {
      $("#lcase").removeClass("fa-check");
      $("#lcase").addClass("fa-times");
      $("#lcase").css("color", "#FF0004");
    }

    if (num.test($("#password1").val())) {
      $("#num").removeClass("fa-times");
      $("#num").addClass("fa-check");
      $("#num").css("color", "#00A41E");
    } else {
      $("#num").removeClass("fa-check");
      $("#num").addClass("fa-times");
      $("#num").css("color", "#FF0004");
    }

    if ($("#password1").val() == $("#password2").val() && $("#password2").val() != "" &&
      num.test($("#password1").val()) && lcase.test($("#password1").val()) &&
      ucase.test($("#password1").val()) && $("#password1").val().length >= 8) {
      $("#pwmatch").removeClass("fa-times");
      $("#pwmatch").addClass("fa-check");
      $("#pwmatch").css("color", "#00A41E");
      $("#updatePassword").removeClass("disabled");
    } else {
      $("#pwmatch").removeClass("fa-check");
      $("#pwmatch").addClass("fa-times");
      $("#pwmatch").css("color", "#FF0004");
    }
  });
</script>

</html>
