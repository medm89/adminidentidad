<!DOCTYPE html>
<html>
<?php 
session_start();
/*Incluir archivos de configuración de CAS*/
include_once './include/cas/config.php';
include_once './include/cas/CAS.php';

/*Inicializar CAS*/
//phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context, false);
// For quick testing you can disable SSL validation of the CAS server. 
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION. 
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL! 
//phpCAS::setNoCasServerValidation();
//phpCAS::setCasServerCACert();
//phpCAS::forceAuthentication();
/*Validar sesion de CAS*/

/*if(phpCAS::checkAuthentication()){
    $user = phpCAS::getUser();
    
}*/

session_destroy();
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Id Uninorte | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Google Font: Raleway -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- My Style -->
    <link href="dist/css/login.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="https://uninorte.edu.co/" target="blank" class="logo">
                <img src="dist/img/uninorte/CustomLoginLogo.png" class="img-responsive" alt="">
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <form method="post">
                    <div class="input-group mb-3">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" required autocomplete="email" autofocus>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" name="password" id="password" required autocomplete="current-password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12">
                            <button type="submit" id="btn-login-id" class="btn btn-primary btn-block">Acceder</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <div class="clearfix" style="margin-top: 8%;">
                    <a href="#" class="forgot">Olvide mi Contraseña</a>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js"></script>
        <script>
    
    $("#btn-login-id").click(function(){
       event.preventDefault();
       var url = "controllers/loginController.php";
       username = $("#email").val();
       password = $("#password").val();
       var parametros = {"email" : username , "password" : password };
       $.ajax({
       type: "POST",
       url: url,
       data: parametros, // Adjuntar los campos del formulario enviado.
       dataType : 'json',
       beforeSend: function( xhr ) {
        // $('#formulario').html('<img class="ico-charging" src="/wp-content/themes/citylending/assets/images/loading_city.svg" />');
	   },
       success: function( data )
       {  
        json = data["respuesta"];
        if(json==1){
            window.location.href = "home.php";
        }
       },
       error: function(data){
        Swal.fire({icon: 'error',title: 'Oops...',text: 'Las credenciales '});
       },
       complete: function( data ){ 
        //console.log(data);
        return false; // Evitar ejecutar el submit del formulario.
       }
    });
    return false;
    });


    function login(){
  $('html, body').animate({scrollTop:0}, 'slow');
  
    }
</script>
</body>

</html>